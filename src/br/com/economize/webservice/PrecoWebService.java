package br.com.economize.webservice;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;

import br.com.economize.model.Preco;
import br.com.economize.persistence.negocio.Fachada;

@Path("/preco")
public class PrecoWebService {
	
	 @GET
	 @Path("/all")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public List<Preco> sayGetAll() {
		 return Fachada.getInstancia().getPrecoBC().listaTodos();
	     //return listOfCountries;
	 }
	
	@Path("/recupera")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Preco sayGet(Preco json)
			throws JSONException {		
		return  Fachada.getInstancia().getPrecoBC().recupera(json);				
	}
	
	@Path("/recuperapeladata/{data}")
	@GET
	//@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Preco sayGetBy(@PathParam("nome") String data)
			throws JSONException {		
		
		DateFormat formatter = new SimpleDateFormat("MM/dd/yy");
		Date dateFormatada;
		try {
			dateFormatada = (Date)formatter.parse(data);
			return  Fachada.getInstancia().getPrecoBC().recuperaPelaData(dateFormatada);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
	
	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response sayGet(@PathParam("id") String id)
			throws JSONException {
		Integer idrecebido = Integer.parseInt(id);
		Preco r = new Preco();
		r.setId(idrecebido);
		Preco recupera = Fachada.getInstancia().getPrecoBC().recupera(r);		
		return Response.status(200).entity(recupera).build();
	}
	 
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/cadastralista")
	public List<Preco> sayInsert(List<Preco> json) throws JSONException {

		System.out.println(json.toString());		
		for (int i = 0; i < json.size(); i++) {
			Fachada.getInstancia().getPrecoBC().cadastrar(json.get(i));
		}		
		return json;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/cadastra")
	public Preco sayInsert(Preco json) throws JSONException {
		Fachada.getInstancia().getPrecoBC().cadastrar(json);
		return json;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/atualiza")
	public Preco sayUpdate(Preco json) throws JSONException {
		Fachada.getInstancia().getPrecoBC().update(json);
		return json;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/deleta")
	public Preco sayDelete(Preco json) throws JSONException {
		Fachada.getInstancia().getPrecoBC().remove(json);
		return json;
	}
	
	
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService(InputStream incomingData) {
		String result = "RESTService Successfully started..";
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}

	
}
