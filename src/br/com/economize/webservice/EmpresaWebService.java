package br.com.economize.webservice;

import java.io.InputStream;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;

import br.com.economize.model.Empresa;
import br.com.economize.persistence.negocio.Fachada;

@Path("/empresa")
public class EmpresaWebService {
	
	 @GET
	 @Path("/all")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public List<Empresa> sayGetAll() {
		 return Fachada.getInstancia().getEmpresaBC().listaTodos();
	     //return listOfCountries;
	 }
	
	@Path("/recupera")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Empresa sayGet(Empresa json)
			throws JSONException {		
		return  Fachada.getInstancia().getEmpresaBC().recupera(json);				
	}
	
	@Path("/recuperapelocnpj/{cnpj}")
	@GET
	//@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Empresa sayGetByCnpj(@PathParam("cnpj") String cnpj)
			throws JSONException {		
		return  Fachada.getInstancia().getEmpresaBC().recuperaPeloCnpj(cnpj);
	}
	
	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response sayGet(@PathParam("id") String id)
			throws JSONException {
		Integer idrecebido = Integer.parseInt(id);
		Empresa r = new Empresa();
		r.setId(idrecebido);
		//System.out.println("id recebido;");
		//System.out.println(id);
		Empresa recupera = Fachada.getInstancia().getEmpresaBC().recupera(r);		
		return Response.status(200).entity(recupera).build();
	}
	 
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/cadastralista")
	public List<Empresa> sayInsert(List<Empresa> json) throws JSONException {

		System.out.println(json.toString());		
		for (int i = 0; i < json.size(); i++) {
			Fachada.getInstancia().getEmpresaBC().cadastrar(json.get(i));
		}		
		return json;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/cadastra")
	public Empresa sayInsert(Empresa json) throws JSONException {
		Fachada.getInstancia().getEmpresaBC().cadastrar(json);
		return json;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/atualiza")
	public Empresa sayUpdate(Empresa json) throws JSONException {
		Fachada.getInstancia().getEmpresaBC().update(json);
		return json;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/deleta")
	public Empresa sayDelete(Empresa json) throws JSONException {
		Fachada.getInstancia().getEmpresaBC().remove(json);
		return json;
	}
	
	
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService(InputStream incomingData) {
		String result = "RESTService Successfully started..";
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}

	
}
