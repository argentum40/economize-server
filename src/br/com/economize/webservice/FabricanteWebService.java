package br.com.economize.webservice;

import java.io.InputStream;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;

import br.com.economize.model.Fabricante;
import br.com.economize.persistence.negocio.Fachada;

@Path("/fabricante")
public class FabricanteWebService {
	
	 @GET
	 @Path("/all")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public List<Fabricante> sayGetAll() {
		 return Fachada.getInstancia().getFabricanteBC().listaTodos();
	     //return listOfCountries;
	 }
	
	@Path("/recupera")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Fabricante sayGet(Fabricante json)
			throws JSONException {		
		return  Fachada.getInstancia().getFabricanteBC().recupera(json);				
	}
	
	@Path("/recuperapelonome/{nome}")
	@GET
	//@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Fabricante sayGetBy(@PathParam("nome") String nome)
			throws JSONException {		
		return  Fachada.getInstancia().getFabricanteBC().recuperaPeloNome(nome);
	}
	
	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response sayGet(@PathParam("id") String id)
			throws JSONException {
		Integer idrecebido = Integer.parseInt(id);
		Fabricante r = new Fabricante();
		r.setId(idrecebido);
		Fabricante recupera = Fachada.getInstancia().getFabricanteBC().recupera(r);		
		return Response.status(200).entity(recupera).build();
	}
	 
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/cadastralista")
	public List<Fabricante> sayInsert(List<Fabricante> json) throws JSONException {

		System.out.println(json.toString());		
		for (int i = 0; i < json.size(); i++) {
			Fachada.getInstancia().getFabricanteBC().cadastrar(json.get(i));
		}		
		return json;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/cadastra")
	public Fabricante sayInsert(Fabricante json) throws JSONException {
		Fachada.getInstancia().getFabricanteBC().cadastrar(json);
		return json;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/atualiza")
	public Fabricante sayUpdate(Fabricante json) throws JSONException {
		Fachada.getInstancia().getFabricanteBC().update(json);
		return json;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/deleta")
	public Fabricante sayDelete(Fabricante json) throws JSONException {
		Fachada.getInstancia().getFabricanteBC().remove(json);
		return json;
	}
	
	
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService(InputStream incomingData) {
		String result = "RESTService Successfully started..";
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}

	
}
