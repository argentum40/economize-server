package br.com.economize.webservice;

import java.io.InputStream;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;

import br.com.economize.model.Usuario;
import br.com.economize.persistence.negocio.Fachada;

@Path("/usuario")
public class UsuarioWebService {
	
	 @GET
	 @Path("/all")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON })
	 public List<Usuario> sayGetAll() {
		 return Fachada.getInstancia().getUsuarioBC().listaTodos();
	     //return listOfCountries;
	 }
	
	@Path("/recupera")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Usuario sayGet(Usuario json)
			throws JSONException {		
		return  Fachada.getInstancia().getUsuarioBC().recupera(json);				
	}
	
	@Path("/recuperapeloemail/{email}")
	@GET
	//@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Usuario sayGetByEmail(@PathParam("email") String email)
			throws JSONException {	
		return  Fachada.getInstancia().getUsuarioBC().recuperaPeloEmail(email);		
	}
	
	
	@Path("/recuperapelocpf/{cpf}")
	@GET
	//@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Usuario sayGetByCnpj(@PathParam("cpf") String cpf)
			throws JSONException {		
		return  Fachada.getInstancia().getUsuarioBC().recuperaPeloCnpj(cpf);
	}
	
	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response sayGet(@PathParam("id") String id)
			throws JSONException {
		Integer idrecebido = Integer.parseInt(id);
		Usuario r = new Usuario();
		r.setId(idrecebido);
		//System.out.println("id recebido;");
		//System.out.println(id);
		Usuario recupera = Fachada.getInstancia().getUsuarioBC().recupera(r);		
		return Response.status(200).entity(recupera).build();
	}
	 
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/cadastralista")
	public List<Usuario> sayInsert(List<Usuario> json) throws JSONException {

		System.out.println(json.toString());		
		for (int i = 0; i < json.size(); i++) {
			Fachada.getInstancia().getUsuarioBC().cadastrar(json.get(i));
		}		
		return json;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/cadastra")
	public Usuario sayInsert(Usuario json) throws JSONException {
		Fachada.getInstancia().getUsuarioBC().cadastrar(json);
		return json;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/atualiza")
	public Usuario sayUpdate(Usuario json) throws JSONException {
		Fachada.getInstancia().getUsuarioBC().update(json);
		return json;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/deleta")
	public Usuario sayDelete(Usuario json) throws JSONException {
		Fachada.getInstancia().getUsuarioBC().remove(json);
		return json;
	}
	
	
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService(InputStream incomingData) {
		String result = "RESTService Successfully started..";
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}

	
}
