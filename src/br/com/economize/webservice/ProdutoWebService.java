package br.com.economize.webservice;

import java.io.InputStream;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;

import br.com.economize.model.Produto;
import br.com.economize.persistence.negocio.Fachada;

@Path("/produto")
public class ProdutoWebService {
	
	 @GET
	 @Path("/all")
	 @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	 public List<Produto> sayGetAll() {
		 return Fachada.getInstancia().getProdutoBC().listaTodos();
	     //return listOfCountries;
	 }
	
	@Path("/recupera")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Produto sayGet(Produto json)
			throws JSONException {		
		return  Fachada.getInstancia().getProdutoBC().recupera(json);				
	}
	
	@Path("/recuperapelocodbarras/{codbarras}")
	@GET
	//@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Produto sayGetBy(@PathParam("codbarras") String codbarras)
			throws JSONException {		
		return  Fachada.getInstancia().getProdutoBC().recuperaPeloCodbarras(codbarras);
	}
	
	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response sayGet(@PathParam("id") String id)
			throws JSONException {
		Integer idrecebido = Integer.parseInt(id);
		Produto r = new Produto();
		r.setId(idrecebido);
		Produto recupera = Fachada.getInstancia().getProdutoBC().recupera(r);		
		return Response.status(200).entity(recupera).build();
	}
	 
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/cadastralista")
	public List<Produto> sayInsert(List<Produto> json) throws JSONException {

		System.out.println(json.toString());		
		for (int i = 0; i < json.size(); i++) {
			Fachada.getInstancia().getProdutoBC().cadastrar(json.get(i));
		}		
		return json;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/cadastra")
	public Produto sayInsert(Produto json) throws JSONException {
		Fachada.getInstancia().getProdutoBC().cadastrar(json);
		return json;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/atualiza")
	public Produto sayUpdate(Produto json) throws JSONException {
		Fachada.getInstancia().getProdutoBC().update(json);
		return json;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/deleta")
	public Produto sayDelete(Produto json) throws JSONException {
		Fachada.getInstancia().getProdutoBC().remove(json);
		return json;
	}
	
	
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService(InputStream incomingData) {
		String result = "RESTService Successfully started..";
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}

	
}
