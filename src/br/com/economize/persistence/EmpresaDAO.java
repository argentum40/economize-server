package br.com.economize.persistence;

import br.com.economize.model.Empresa;
import br.com.economize.persistence.interfaces.GenericDAO;
import br.com.economize.persistence.interfaces.IEmpresaDAO;

/**
 * @author rodrigo.venancio
 * 
 */
public class EmpresaDAO extends GenericDAO<Empresa> implements IEmpresaDAO {

	private static final long serialVersionUID = -7307696164662037283L;

	@Override
	public Empresa recuperaPeloCnpj(String cnpj) {
	
		Empresa o = null;
		try {
			o = (Empresa) getEntityManager().createQuery("SELECT u FROM Empresa u WHERE u.cnpj = ?1")
					.setParameter(1, cnpj).getSingleResult();
			if (o == null) {
				System.out.println("null no banco");
			}

		} catch (Exception e) {
			o = null;
			System.err.println(e.getMessage());
			return o;
		} finally {
			closeEntityManager();
		}
		return o;		
	}

}
