package br.com.economize.persistence.interfaces;

import br.com.economize.model.Usuario;

public interface IUsuarioDAO extends IGenericDAO<Usuario>{
	
	public Usuario recuperaPeloCpf(String cpf);
	public Usuario recuperaPeloEmail(String email);
	
}
