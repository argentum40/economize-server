package br.com.economize.persistence.interfaces;

import br.com.economize.model.Empresa;

public interface IEmpresaDAO extends IGenericDAO<Empresa>{
	
	public Empresa recuperaPeloCnpj(String cnpj);
	
}
