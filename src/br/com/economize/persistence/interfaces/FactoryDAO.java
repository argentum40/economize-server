package br.com.economize.persistence.interfaces;

import java.util.HashMap;
import java.util.Map;

/**
 * Classe que representa uma fabrica de repositorios. Serve para se obter uma instancia de qualquer 
 * repositorio. Tambem gerencia as instancias ja criadas dos repositorios, para evitar sobrecarregar
 * a memoria com mais de uma instancia.
 */
public class FactoryDAO {

	private static FactoryDAO instancia;
	private Map<String, Object> daos;
	
	private FactoryDAO() {
		this.daos = new HashMap<String, Object>();
	}
	
	/**
	 * Obtem uma instancia singleton da fabrica.
	 * @return FactoryDAO
	 */
	public static synchronized FactoryDAO getInstancia() {
		if (instancia == null) {
			instancia = new FactoryDAO();
		}
		return instancia;
	}
	
	/**
	 * Responsavel por recuperar uma instancia de qualquer repositorio. 
	 *
	 * @param classe - Classe qualquer, referente a um repositorio.
	 * @return T - Instancia do repositorio.
	 */
	public <T> T getDAO(Class<T> classe) {
		try {
			if (!daos.containsKey(classe.getName())) {
				daos.put(classe.getName(), Class.forName(classe.getName()).newInstance());
			} 
			return classe.cast(daos.get(classe.getName()));
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}