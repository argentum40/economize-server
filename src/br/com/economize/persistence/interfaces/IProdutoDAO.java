package br.com.economize.persistence.interfaces;

import br.com.economize.model.Produto;

public interface IProdutoDAO extends IGenericDAO<Produto>{
	
	public Produto recuperaPeloCodbarras(String codbarras);
	
}
