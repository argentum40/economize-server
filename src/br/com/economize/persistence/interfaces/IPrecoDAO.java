package br.com.economize.persistence.interfaces;

import java.util.Date;

import br.com.economize.model.Preco;

public interface IPrecoDAO extends IGenericDAO<Preco>{
	
	public Preco recuperaPelaData(Date data);
	
}
