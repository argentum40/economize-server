package br.com.economize.persistence.interfaces;

import br.com.economize.model.Fabricante;

public interface IFabricanteDAO extends IGenericDAO<Fabricante>{
	
	public Fabricante recuperaPeloNome(String nome);
	
}
