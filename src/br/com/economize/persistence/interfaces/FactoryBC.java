package br.com.economize.persistence.interfaces;

import java.util.HashMap;
import java.util.Map;

/**
 * Classe que representa uma fabrica de cadastros. Serve para se obter uma instancia de qualquer 
 * cadastro. Tambem gerencia as instancias ja criadas dos cadastros, para evitar sobrecarregar a 
 * memoria com mais de uma instancia.
 */
public class FactoryBC {

	private static FactoryBC instancia;
	private Map<String, Object> bcs;
	
	private FactoryBC() {
		this.bcs = new HashMap<String, Object>();
	}
	
	/**
	 * Obtem uma instancia singleton da fabrica.
	 * 
	 * @return CadastroFactory
	 */
	public static synchronized FactoryBC getInstancia() {
		if (instancia == null) {
			instancia = new FactoryBC();
		}
		return instancia;
	}
	
	/**
	 * Responsavel por recuperar uma instancia de qualquer cadastro. 
	 * 
	 * @param classe - Classe qualquer, referente a um cadastro.
	 * @return - Instancia de cadastro.
	 */
	public <T> T getBC(Class<T> classe) {
		try {
			if (!bcs.containsKey(classe.getName())) {
				bcs.put(classe.getName(), Class.forName(classe.getName()).newInstance());
			} 
			return classe.cast(bcs.get(classe.getName()));
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

}