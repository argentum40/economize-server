package br.com.economize.persistence.negocio;

import br.com.economize.persistence.interfaces.FactoryBC;

public class Fachada {
	
	private static Fachada instancia = null;
	
	public static Fachada getInstancia() {
		if (Fachada.instancia == null) {
			synchronized (Fachada.class) {
				if (Fachada.instancia == null) {
					Fachada.instancia = new Fachada();
				}
			}
		}
		return Fachada.instancia;
	}	
	
	public EmpresaBC getEmpresaBC() {
		return FactoryBC.getInstancia().getBC(EmpresaBC.class);
	}
	
	public FabricanteBC getFabricanteBC() {
		return FactoryBC.getInstancia().getBC(FabricanteBC.class);
	}
	
	public PrecoBC getPrecoBC() {
		return FactoryBC.getInstancia().getBC(PrecoBC.class);
	}


	public ProdutoBC getProdutoBC() {
		return FactoryBC.getInstancia().getBC(ProdutoBC.class);
	}
	
	public UsuarioBC getUsuarioBC() {
		return FactoryBC.getInstancia().getBC(UsuarioBC.class);
	}
	
	

	
}