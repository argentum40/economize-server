package br.com.economize.persistence.negocio;

import java.io.Serializable;
import java.util.List;

import br.com.economize.model.Empresa;
import br.com.economize.persistence.EmpresaDAO;
import br.com.economize.persistence.interfaces.FactoryDAO;
import br.com.economize.persistence.interfaces.IEmpresaDAO;




/**
 * @author rodrigo.venancio
 *
 */
public class EmpresaBC implements Serializable {

	private static final long serialVersionUID = 7958030368252958131L;

	private IEmpresaDAO getEmpresaIDAO() {
		return FactoryDAO.getInstancia().getDAO(EmpresaDAO.class);
	}	
	
	public void cadastrar(Empresa r){
		getEmpresaIDAO().insert(r);
	}
	public void remove(Empresa r){
		getEmpresaIDAO().delete(r);
	}
	public void update(Empresa r){
		getEmpresaIDAO().update(r);
	}
	public Empresa recupera(Empresa r){
		return getEmpresaIDAO().findByID(r.getId());
	}	
	public List<Empresa> listaTodos(){
		return getEmpresaIDAO().findAll();
	}
	
	public Empresa recuperaPeloCnpj(String cnpj){
		return getEmpresaIDAO().recuperaPeloCnpj(cnpj);
	}

	
	
	
}