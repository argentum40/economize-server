package br.com.economize.persistence.negocio;

import java.io.Serializable;
import java.util.List;

import br.com.economize.model.Usuario;
import br.com.economize.persistence.UsuarioDAO;
import br.com.economize.persistence.interfaces.FactoryDAO;
import br.com.economize.persistence.interfaces.IUsuarioDAO;


/**
 * @author rodrigo.venancio
 *
 */
public class UsuarioBC implements Serializable {

	private static final long serialVersionUID = 7958030368252958131L;

	private IUsuarioDAO getUsuarioIDAO() {
		return FactoryDAO.getInstancia().getDAO(UsuarioDAO.class);
	}	
	
	public void cadastrar(Usuario r){
		getUsuarioIDAO().insert(r);
	}
	public void remove(Usuario r){
		getUsuarioIDAO().delete(r);
	}
	public void update(Usuario r){
		getUsuarioIDAO().update(r);
	}
	public Usuario recupera(Usuario r){
		return getUsuarioIDAO().findByID(r.getId());
	}	
	public List<Usuario> listaTodos(){
		return getUsuarioIDAO().findAll();
	}
	
	public Usuario recuperaPeloCnpj(String cpf){
		return getUsuarioIDAO().recuperaPeloCpf(cpf);
	}
	
	public Usuario recuperaPeloEmail(String email){
		return getUsuarioIDAO().recuperaPeloEmail(email);
	}

}