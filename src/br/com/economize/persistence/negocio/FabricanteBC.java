package br.com.economize.persistence.negocio;

import java.io.Serializable;
import java.util.List;

import br.com.economize.model.Fabricante;
import br.com.economize.persistence.FabricanteDAO;
import br.com.economize.persistence.interfaces.FactoryDAO;
import br.com.economize.persistence.interfaces.IFabricanteDAO;




/**
 * @author rodrigo.venancio
 *
 */
public class FabricanteBC implements Serializable {

	private static final long serialVersionUID = 7958030368252958131L;

	private IFabricanteDAO getFabricanteIDAO() {
		return FactoryDAO.getInstancia().getDAO(FabricanteDAO.class);
	}	
	
	public void cadastrar(Fabricante r){
		getFabricanteIDAO().insert(r);
	}
	public void remove(Fabricante r){
		getFabricanteIDAO().delete(r);
	}
	public void update(Fabricante r){
		getFabricanteIDAO().update(r);
	}
	public Fabricante recupera(Fabricante r){
		return getFabricanteIDAO().findByID(r.getId());
	}	
	public List<Fabricante> listaTodos(){
		return getFabricanteIDAO().findAll();
	}
	
	public Fabricante recuperaPeloNome(String nome){
		return getFabricanteIDAO().recuperaPeloNome(nome);
	}

	
	
	
}