package br.com.economize.persistence.negocio;

import java.io.Serializable;
import java.util.List;

import br.com.economize.model.Produto;
import br.com.economize.persistence.ProdutoDAO;
import br.com.economize.persistence.interfaces.FactoryDAO;
import br.com.economize.persistence.interfaces.IProdutoDAO;




/**
 * @author rodrigo.venancio
 *
 */
public class ProdutoBC implements Serializable {

	private static final long serialVersionUID = 7958030368252958131L;

	private IProdutoDAO getProdutoIDAO() {
		return FactoryDAO.getInstancia().getDAO(ProdutoDAO.class);
	}	
	
	public void cadastrar(Produto r){
		getProdutoIDAO().insert(r);
	}
	public void remove(Produto r){
		getProdutoIDAO().delete(r);
	}
	public void update(Produto r){
		getProdutoIDAO().update(r);
	}
	public Produto recupera(Produto r){
		return getProdutoIDAO().findByID(r.getId());
	}	
	public List<Produto> listaTodos(){
		return getProdutoIDAO().findAll();
	}
	
	public Produto recuperaPeloCodbarras(String codbarras){
		return getProdutoIDAO().recuperaPeloCodbarras(codbarras);
	}

	
	
	
}