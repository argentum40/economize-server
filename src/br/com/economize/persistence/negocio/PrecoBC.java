package br.com.economize.persistence.negocio;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import br.com.economize.model.Preco;
import br.com.economize.persistence.PrecoDAO;
import br.com.economize.persistence.interfaces.FactoryDAO;
import br.com.economize.persistence.interfaces.IPrecoDAO;




/**
 * @author rodrigo.venancio
 *
 */
public class PrecoBC implements Serializable {

	private static final long serialVersionUID = 7958030368252958131L;

	private IPrecoDAO getPrecoIDAO() {
		return FactoryDAO.getInstancia().getDAO(PrecoDAO.class);
	}	
	
	public void cadastrar(Preco r){
		getPrecoIDAO().insert(r);
	}
	public void remove(Preco r){
		getPrecoIDAO().delete(r);
	}
	public void update(Preco r){
		getPrecoIDAO().update(r);
	}
	public Preco recupera(Preco r){
		return getPrecoIDAO().findByID(r.getId());
	}	
	public List<Preco> listaTodos(){
		return getPrecoIDAO().findAll();
	}
	
	public Preco recuperaPelaData(Date data){
		return getPrecoIDAO().recuperaPelaData(data);	
		
}		
}