package br.com.economize.persistence;

import java.util.Date;

import br.com.economize.model.Preco;
import br.com.economize.persistence.interfaces.GenericDAO;
import br.com.economize.persistence.interfaces.IPrecoDAO;

/**
 * @author rodrigo.venancio
 * 
 */
public class PrecoDAO extends GenericDAO<Preco> implements IPrecoDAO {

	private static final long serialVersionUID = -7307696164662037283L;

	@Override
	public Preco recuperaPelaData(Date data) {
	
		Preco o = null;
		try {
			o = (Preco) getEntityManager().createQuery("SELECT u FROM Preco u WHERE u.data = ?1")
					.setParameter(1, data).getSingleResult();
			if (o == null) {
				System.out.println("null no banco");
			}

		} catch (Exception e) {
			o = null;
			System.err.println(e.getMessage());
			return o;
		} finally {
			closeEntityManager();
		}
		return o;		
	}

}
