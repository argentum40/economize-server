package br.com.economize.persistence;

import br.com.economize.model.Usuario;
import br.com.economize.persistence.interfaces.GenericDAO;
import br.com.economize.persistence.interfaces.IUsuarioDAO;

/**
 * @author rodrigo.venancio
 * 
 */
public class UsuarioDAO extends GenericDAO<Usuario> implements IUsuarioDAO {

	private static final long serialVersionUID = -7307696164662037283L;

	@Override
	public Usuario recuperaPeloCpf(String cpf) {
	
		Usuario o = null;
		try {
			o = (Usuario) getEntityManager().createQuery("SELECT u FROM Usuario u WHERE u.cpf = ?1")
					.setParameter(1, cpf).getSingleResult();
			if (o == null) {
				System.out.println("null no banco");
			}

		} catch (Exception e) {
			o = null;
			System.err.println(e.getMessage());
			return o;
		} finally {
			closeEntityManager();
		}
		return o;		
	}
	
	
	@Override
	public Usuario recuperaPeloEmail(String email) {
	
		Usuario o = null;
		try {
			o = (Usuario) getEntityManager().createQuery("SELECT u FROM Usuario u WHERE u.email = ?1")
					.setParameter(1, email).getSingleResult();
			if (o == null) {
				System.out.println("null no banco");
			}

		} catch (Exception e) {
			o = null;
			System.err.println(e.getMessage());
			return o;
		} finally {
			closeEntityManager();
		}
		return o;		
	}

}
