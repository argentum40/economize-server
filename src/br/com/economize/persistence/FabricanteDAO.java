package br.com.economize.persistence;

import br.com.economize.model.Fabricante;
import br.com.economize.persistence.interfaces.GenericDAO;
import br.com.economize.persistence.interfaces.IFabricanteDAO;

/**
 * @author rodrigo.venancio
 * 
 */
public class FabricanteDAO extends GenericDAO<Fabricante> implements IFabricanteDAO {

	private static final long serialVersionUID = -7307696164662037283L;

	@Override
	public Fabricante recuperaPeloNome(String nome) {
	
		Fabricante o = null;
		try {
			o = (Fabricante) getEntityManager().createQuery("SELECT u FROM Fabricante u WHERE u.nome = ?1")
					.setParameter(1, nome).getSingleResult();
			if (o == null) {
				System.out.println("null no banco");
			}

		} catch (Exception e) {
			o = null;
			System.err.println(e.getMessage());
			return o;
		} finally {
			closeEntityManager();
		}
		return o;		
	}

}
