package br.com.economize.persistence;

import br.com.economize.model.Produto;
import br.com.economize.persistence.interfaces.GenericDAO;
import br.com.economize.persistence.interfaces.IProdutoDAO;

/**
 * @author rodrigo.venancio
 * 
 */
public class ProdutoDAO extends GenericDAO<Produto> implements IProdutoDAO {

	private static final long serialVersionUID = -7307696164662037283L;

	@Override
	public Produto recuperaPeloCodbarras(String codbarras) {
	
		Produto o = null;
		try {
			o = (Produto) getEntityManager().createQuery("SELECT u FROM Produto u WHERE u.codbarras = ?1")
					.setParameter(1, codbarras).getSingleResult();
			if (o == null) {
				System.out.println("null no banco");
			}

		} catch (Exception e) {
			o = null;
			System.err.println(e.getMessage());
			return o;
		} finally {
			closeEntityManager();
		}
		return o;		
	}

}
