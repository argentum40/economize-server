package br.com.economize.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name="produto")
@XmlRootElement
public class Produto {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String nome;
	private String codbarras;
	@ManyToOne
	@JoinColumn(name="id_fabricante", referencedColumnName="id")
	//private Categoria category;
	private Fabricante fabricante;
	
	@ManyToMany
    @JoinTable(name="produto_has_empresas", joinColumns=
    {@JoinColumn(name="produto_id")}, inverseJoinColumns=
      {@JoinColumn(name="empresa_id")})
	private List<Empresa> empresas;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCodbarras() {
		return codbarras;
	}
	public void setCodbarras(String codbarras) {
		this.codbarras = codbarras;
	}
	public Fabricante getFabricante() {
		return fabricante;
	}
	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
	}
	public List<Empresa> getEmpresa() {
		return empresas;
	}
	public void setEmpresa(List<Empresa> empresa) {
		this.empresas = empresa;
	}
	
	

}
